# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = Hash.new
  str.split.each do |word|
    word_lengths[word] = word.length
  end
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |key, value| value }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  older.merge!(newer)
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counts = Hash.new(0)
  word.each_char { |char| letter_counts[char] += 1  }
  letter_counts
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniqs = Hash.new(0)
  arr.each { |el| uniqs[el] += 1 }
  uniqs.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  odds_evens = Hash.new(0)
  numbers.each do |num|
    odds_evens[:odd] += 1 if num.odd?
    odds_evens[:even] += 1 if num.even?
  end
  odds_evens
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  # Reverse sort string since otherwise when calling sort_by value on the hash later, it won't account for sorting (reverse) alphabetically also (earlier in the alphabet, later in the array).
  string.chars.sort.reverse.each do |char|
    vowel_count[char] += 1 if "aeiou".include?(char)
  end

  vowel_count.sort_by { |vowel, count| count }.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  # Iterate over hash and create array "names" of all students whose birthday is in the second half of the year
  names = []
  students.each do |name, month|
    names << name if month > 6
  end

  # Create array of all pair combinations of students returned above. Slow pointer i moves from the beginning of the array, fast pointer j from the end, until all entries are covered and pushed into the unique combinations array, which is then returned.
  combos = []
  i = 0
  while i < names.length
    j = names.length - 1
    until i == j
      combos << [ names[i], names[j] ]
      j -= 1
    end
    i += 1
  end
  combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  # Create count hash with species as keys and population size as values. Use the max and min values to find largest adn smallest population size, and the hash's length to determine number_of_species. Run the formula.
  species_count = Hash.new(0)
  specimens.each { |species, count| species_count[species] += 1 }

  number_of_species = species_count.length
  smallest_population_size = species_count.values.min
  largest_population_size = species_count.values.max

  number_of_species**2 * smallest_population_size / largest_population_size
end


# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal = character_count(normal_sign)
  tweaked = character_count(vandalized_sign)

  tweaked.all? do |char, count|
    normal.key?(char) && normal[char] >= count
  end
end

def character_count(str)
  clean_str = remove_punctuation(str)

  chars_count = Hash.new(0)
  clean_str.chars.each { |char, count| chars_count[char] += 1 }
  chars_count
end

def remove_punctuation(str)
  str.downcase.gsub /\W+/, ' '
end
